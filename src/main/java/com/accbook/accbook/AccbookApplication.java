package com.accbook.accbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccbookApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccbookApplication.class, args);
	}

}
